(ns zimba-api.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s]
            [zimba-api.services.infrastructure :refer [ok-success wrap-logging log-request-exception exception-500-handler database-exception-422-handler validation-exception-400-handler]]
            [compojure.route :as route]
            [compojure.api.exception :as ex]

            [ring.util.http-response :as response]
            [ring.middleware.cors :refer [wrap-cors]]
            [clojure.tools.logging :as log]
            ))

(def base-path "/zimba-api")



(def app
  (-> (api
        {
         :exceptions
         {:handlers
          {
           ;; 400
           ::ex/request-validation validation-exception-400-handler

           ;; 500
           ::ex/default            (exception-500-handler response/internal-server-error "problem:internal-server-error")

           }}
         :swagger
         {:ui   "/"
          :spec "/swagger.json"
          :data {:info {:title       "Zimba-api"
                        :description ""}
                 :tags [{:name "api", :description "some apis"}]}}}


        (context base-path []
          :tags ["api"]
          :middleware [wrap-logging]


          (GET "/hotels-near/:longitude/:latitude/:distance" []
            ;  :path-params [
            ;                longitude :- s/Num
            ;                latitude :- s/Num
            ;                distance :- s/Int
            ;                ]
            :header-params [accept-language :- String]
            :summary "hotels near a point"
            ;:return nearby-use-case/Response
            ;(ok-success {:hotels (nearby-use-case/execute longitude latitude distance accept-language)} {"Content-Language" accept-language})
            (ok-success {:hello "world"} {"Content-Language" accept-language})
            )

          (undocumented
            (route/not-found (ok-success {:not "found"} {})))

          )
        )
        (wrap-cors
          :access-control-allow-origin [#".*"]
          :access-control-allow-headers ["Origin" "X-Requested-With"
                                         "Content-Type" "Accept"]
          :access-control-allow-methods [:get :put :post :delete :options]
          )
      )
  )




(comment




  (-> {:uri            "/zimba-api/hotels-near/-0.157848/51.51585/15"
       :request-method :get
       :headers        {:accept-language "en-us"}
       }
      app
      :body
      slurp
      (cheshire.core/parse-string true))




  )