(ns zimba-api.services.infrastructure
  (:require [clojure.tools.logging :as log]
            [compojure.api.coercion.core :as cc]
            [ring.util.http-response :as response]))


(defn ok-success
  "200 OK (Success)
  OK"
  ([] (ok-success nil {}))
  ([body headers]
   {:status  200
    :headers (merge {"Vary" "Accept-Language"} headers)
    :body    body}))


(defn- get-req-body! [{:keys [body] :as request}]
  (if (nil? body) "" (:params request)))

(defn request-log [request]
  {:url          (:uri request)
   :headers      {:accept-language (-> request
                                       :headers
                                       (get "accept-language"))}
   :body (get-req-body! request)
   :query-params (:query-params request)
   }
  )

(defn log-request-exception [request exception]
  (log/error {:service (:request-method request)
              :details {:request   (request-log request)
                        :exception exception}})
  )

(defn log-request-response [request response]
  (log/info {:service (:request-method request)
             :details {:request  (request-log request)
                       :response (select-keys response [:status :body])}}))


(defn wrap-logging [handler]
  (fn inner-wrap-logging
    ([request]
     (let [response (handler request)]
       (log-request-response request response)
       response))

    ([request respond raise]
     (let [pre-response (handler request)
           response (respond pre-response)]
       (log-request-response request pre-response)

       response))))


(defn exception-500-handler [f type]
  (fn inner-exception-500-handler [^Exception e data request]
    (log-request-exception request e)
    (f {:detail (.getMessage e) :title "There was an unknown error" :type type})))


(defn database-exception-422-handler
  [e data req]
  (response/unprocessable-entity
    (-> data
        (dissoc :request)
        ((fn transform-response [response]
           (log-request-response req response)
           {
            :title  "problem/database-exception",
            :type   (:type response)
            :detail response
            }
           )))))

(defn validation-exception-400-handler
  [e data req]
  (response/bad-request
    (-> data
        (dissoc :request)
        (update :coercion cc/get-name)
        (->> (cc/encode-error (:coercion data)))
        ((fn transform-response [response]
           (log-request-response req response)
           {
            :title  "problem:request-validation",
            :type   (:type response)
            :detail response
            }
           )))))




