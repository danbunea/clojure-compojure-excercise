(ns zimba-api.services.http-service
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json]
            [clojure.core.async :refer :all]
            [clojure.tools.logging :as log]))




(defn <GET
  ([url] (<GET url {}))
  ([url options]
   (let [response-channel (chan)]
     (http/get
       url
       options
       (fn [{:keys [status headers body error opts]}]
         (if error
           (put! response-channel error)
           (put! response-channel (json/read-str body :key-fn keyword)))))
     response-channel)))


(defn GET
  ([url] (GET url {}))
  ([url options]
   (let [{:keys [status headers body error] :as resp} @(http/get url options)]
     (if error
       (log/error {:service  ::GET
                   :request  {:url     url
                              :options options}
                   :response (select-keys resp [:error :status :body])})

       (do
         (log/debug {:service  ::GET
                    :request  {:url     url
                               :options options}
                    :response (select-keys resp [:status :body])})
         (-> body
             (json/read-str :key-fn keyword))
         )))))



(comment
  (GET "https://nwebdevlive.radissonhotels.com/content-api/internal/hotels?offset=0&limit=2" {:headers {"Accept-Language" "en-us"}})

  (go
    (print (<! (<GET "https://nwebdevlive.radissonhotels.com/content-api/internal/hotels?offset=0&limit=2"
                     {:headers {"Accept-Language" "en-us"}})
               )))

  )
