(ns zimba-api.services.google-api
  (:require [config.core :refer [env]]
            [zimba-api.services.http-service :refer [GET]]
            [clojure.tools.logging :as log]
            [ring.util.codec :refer [url-encode]]))


(defn get-city-center-coordinates! [city state country]
  (let [address (url-encode (clojure.string/join "," (if (not (empty? state))
                                           [city state country]
                                           [city country])))
        url (str "https://maps.googleapis.com/maps/api/geocode/json?address=" address "&key=" (:google_api_key env))
        response (try
                    (GET url)
                    (catch Exception e
                      (log/error {:service ::get-city-center-coordinates! :details {:url url :error (.getMessage e)}})
                      ))
        google_coordinates (-> response
                               :results
                               first
                               :geometry
                               :location
                               )]
    (if google_coordinates
      {:latitude  (:lat google_coordinates)
       :longitude (:lng google_coordinates)}
      nil
      )
    ))



(def city-center-coordinates! (memoize get-city-center-coordinates!))




(comment


  (GET (str "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6905615,-73.9976592&key=" (:google_api_key env)))
  (GET (str "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6655101,-73.89188969999998&key="  (:google_api_key env)))
  (GET (str "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6655101,-73.89188969999998&key="  (:google_api_key env)))

  (city-center-coordinates! "Dallas" "Texas" "US")
  (city-center-coordinates! "London" "" "UK")


  (city-center-coordinates! "Bismarck" "North Dakota" "United States")
  (city-center-coordinates! "London" nil "UK")
  (city-center-coordinates! "4375437673ndsdfsdfon" nil "Uds2341234aK")



)