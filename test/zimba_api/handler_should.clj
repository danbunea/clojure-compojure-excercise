(ns zimba-api.handler-should
  (:require [clojure.test :refer :all]
            [clojure.data.json :as json]
            [ring.mock.request :as ring-mock]
            [mock-clj.core :refer [with-mock]]

            [zimba-api.handler :refer :all]
            ))

(def headers {"headers" {"Accept-Language" "en-us"}})

(defn parse-body [body]
  (json/read-str (slurp body) :key-fn keyword))




(deftest get-near-by-hotels
  (testing "Test GET request to /zimba-api/hotels-near/-73.948390/40.711089/15"
    (let [results [{:code "FARGO" :latitude 1 :longitude 2}]]
      ;(with-mock [find-hotels-near results]
                 (let [aa (-> (ring-mock/request :get "/zimba-api/hotels-near/-73.948390/40.711089/15")
                              (ring-mock/header "Accept-Language" "en-us"))
                       {:keys [status headers body error] :as response} (app aa)
                       body (parse-body body)

                       ]
                   (is (= (:status response) 200))
                   (is (= {:hotels {:FARGO {:latitude 1 :longitude 2}}} body)))
      ;)
    )))





