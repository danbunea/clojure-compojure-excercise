(ns zimba-api.use-cases.find-city-center-should
  (:require [clojure.test :refer :all]
            [zimba-api.use-cases.find-city-center :refer [execute]]))
