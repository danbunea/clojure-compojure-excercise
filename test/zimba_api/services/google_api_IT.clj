(ns zimba-api.services.google-api-IT
  (:require [clojure.test :refer :all]
            [zimba-api.services.google-api :refer [city-center-coordinates! get-city-center-coordinates!]]))


(deftest retrieve-a-city-center
  (is (not-empty (city-center-coordinates! "Dallas" "Texas" "US")))
  (is (not-empty (city-center-coordinates! "London" "" "UK")))
  (is (not-empty (city-center-coordinates! "London" nil "UK")))
  (is (not-empty (city-center-coordinates! "Bismarck" "North Dakota" "United States")))
  (is (nil? (city-center-coordinates! "4375437673ndsdfsdfon" nil "Uds2341234aK")))
  )


