# zimba-api

A template to create an endpoint in an interactive excercise.

## Tools

### Clojure/Leiningen

From www.leiningen.org install leiningen (preferably in /usr/local/bin).
After following the instructions from the website go to version 2.8.2 

`lein upgrade 2.8.2`.  

### IntelliJ/Cursive

Install the Cursive plugin for IntelliJ.

### Run the server locally

`lein ring server`

### Run the tests

`lein test`

### Packaging and running as standalone jar (with custom config file)


with custom config profile
```
lein with-profile local do clean, ring uberjar
java -Dconfig="config/dev/config.edn" -jar target/zimba-api.jar
```

or

```
lein do clean, ring uberjar
java -jar target/zimba-api.jar
```

### Packaging as war

`lein ring uberwar`

## License

Copyright ©  FIXME