(defproject zimba-api "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [;server
                 [org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "0.4.474"]
                 [metosin/compojure-api "2.0.0-alpha30" :exclude [compojure, metosin/muuntaja]]
                 [ring/ring "1.6.3"]
                 [compojure "1.6.1"]
                 [manifold "0.1.8"]
                 [metosin/spec-tools "0.9.2"]
                 [ring-cors "0.1.13"]
                 [ring/ring-codec "1.1.2"]


                 ;testing
                 [mock-clj "0.2.0"]
                 ;logging
                 [org.clojure/tools.logging "0.4.1"]
                 [ch.qos.logback/logback-classic "1.1.3"]   ; No need to specify slf4j-api, it’s required by logback
                 ;http
                 [http-kit "2.3.0"]
                 ;json
                 [org.clojure/data.json "0.2.6"]
                 ;environements
                 [yogthos/config "1.1.1"]
                 ]
  :local-repo ".m2/repository"                              ;; Override location of the local maven repository. Relative to project root.
  :ring {:handler zimba-api.handler/app
         :async?  true}
  :uberjar-name "zimba-api.jar"


  :profiles {
             :dev      {:resource-paths ["resources/config/dev"]
                        :dependencies   [[http-kit.fake "0.2.1"]
                                         [ring/ring-mock "0.4.0"]]
                        :plugins        [[lein-ring "0.12.0"]]
                        :ring {:port 8080}
                        }
             }
  )
